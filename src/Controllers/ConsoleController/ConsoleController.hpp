#ifndef DEF_CTCONSOLE
#define DEF_CTCONSOLE

/* CTConsole.hpp
 * Defines the class CTConsole
 * CTConsole is a controller which displays a game in a terminal
 * Creators : krilius, manzerbredes
 * Date : 29/04/2915 */

#include <iostream>
#include "../../Helpers/Keyboard.hpp"
#include "../../Model/Game.hpp"

class ConsoleController
{
	private:

		Game m_game;
		kbdh::Direction waitArrowKeyPress();
	public:
		//Constructor and Destructor
		ConsoleController();
		~ConsoleController();

		//Helpers
		void run();
		void coutStats();
};

#endif
