#include "./ConsoleController.hpp"
#include <SFML/Window/Keyboard.hpp>
#include "../../Helpers/Keyboard.hpp"


//==================== Constructor and Destructor  ====================

//Constructor
ConsoleController::ConsoleController()
{
}

//Destructor
ConsoleController::~ConsoleController()
{
}

//==================== Helpers  ====================

//Run the game.
void ConsoleController::run()
{

	//Init keyPress
	kbdh::Direction keyPress;

	//Intruction msg
	std::cout << "Use arrows to play !" << std::endl;

	//Pop a random number on the grid
	m_game.popRandomNumber();

	//First cout stats
	this->coutStats();

	//First cout grid
	m_game.coutGrid();


	//Start game
	while (!m_game.isOver())
	{
		//Get key press
		keyPress=this->waitArrowKeyPress();

		//Apply move
		bool moveDone=m_game.swipe(keyPress);

		//Cout stats
		this->coutStats();

		//Cout grid
		m_game.coutGrid();

	}

	//Last cout stats
	this->coutStats();

	//Last cout grid
	m_game.coutGrid();
}




//Wait for keypress and return the keyPress.
kbdh::Direction ConsoleController::waitArrowKeyPress()
{
	//Initialise keyPress
	kbdh::Direction keyPress;

	//White space to remove arrows print by the terminal
	std::string spaces="                                            ";

	//Wait for keypress
	while(1){
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			keyPress=kbdh::Left;
			while(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				//Wait for release and try to remove arrow printed characters
				std::cout << "\r" << spaces;
			}
			break;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			keyPress=kbdh::Right;
			while(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				//Wait for release and try to remove arrow printed characters
				std::cout << "\r" << spaces;
			}
			break;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			keyPress=kbdh::Up;
			while(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				//Wait for release and try to remove arrow printed characters
				std::cout << "\r" << spaces;
			}
			break;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			keyPress=kbdh::Down;
			while(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				//Wait for release and try to remove arrow printed characters
				std::cout << "\r" << spaces;
			}
			break;
		}
	}

	return keyPress;
}


//Cout the stats of the game
void ConsoleController::coutStats(){

	std::cout << std::endl << "Score : " << m_game.getScore() << std::endl;
	std::cout << "Nombre de coups : " << m_game.getNbMove() << std::endl;
}
