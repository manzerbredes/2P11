


#include <iostream>
#include <string>
#include <SFML/Window.hpp>
#include "../../View/MainWindow.hpp"
#include "../../Model/Game.hpp"
#include "../../Helpers/Keyboard.hpp"
#include <SFML/Window/Keyboard.hpp>

class SFMLController{

	private:
		MainWindow m_MainWindow;
		Game m_game;


	public:
		SFMLController();
		~SFMLController();

		kbdh::Direction waitArrowKeyPress();
		void run();

};
