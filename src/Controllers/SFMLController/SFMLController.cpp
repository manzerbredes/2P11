#include "SFMLController.hpp"






SFMLController::SFMLController() : m_game(), m_MainWindow(600,800, "2P11"){
}


SFMLController::~SFMLController(){

}




void SFMLController::run(){

	kbdh::Direction keyPress;

	m_game.popRandomNumber();
	while(m_MainWindow.isOpen()){


		sf::Event event;
		while (m_MainWindow.pollEvent(event))
		{
			// évènement "fermeture demandée" : on ferme la fenêtre
			if (event.type == sf::Event::Closed)
				m_MainWindow.close();
			if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::Up)
				{
					m_game.swipe(kbdh::Direction::Up);
				}
				if (event.key.code == sf::Keyboard::Down)
				{
					m_game.swipe(kbdh::Direction::Down);
					// Do something when W is pressed...
				}
				if (event.key.code == sf::Keyboard::Left){

					m_game.swipe(kbdh::Direction::Left);
				}
				if (event.key.code == sf::Keyboard::Right){
					m_game.swipe(kbdh::Direction::Right);
				}

				// And so on.
			}
		}


		m_MainWindow.clearBG();
		//m_game.swipe(kbdh::Direction::Left);
		std::vector<std::vector<int> > aaa=m_game.getGrid();
		m_MainWindow.drawGame(aaa,m_game.isOver(), m_game.getStats());
		m_MainWindow.display();

		//keyPress=this->waitArrowKeyPress();
		m_game.swipe(keyPress);
	}


}

//Wait for keypress and return the keyPress.
kbdh::Direction SFMLController::waitArrowKeyPress()
{
	//Initialise keyPress
	kbdh::Direction keyPress;

	//White space to remove arrows print by the terminal
	std::string spaces="                                            ";

	//Wait for keypress
	while(1){
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			keyPress=kbdh::Left;
			while(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				//Wait for release and try to remove arrow printed characters
				std::cout << "\r" << spaces;
			}
			break;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			keyPress=kbdh::Right;
			while(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				//Wait for release and try to remove arrow printed characters
				std::cout << "\r" << spaces;
			}
			break;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			keyPress=kbdh::Up;
			while(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				//Wait for release and try to remove arrow printed characters
				std::cout << "\r" << spaces;
			}
			break;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			keyPress=kbdh::Down;
			while(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				//Wait for release and try to remove arrow printed characters
				std::cout << "\r" << spaces;
			}
			break;
		}
	}

	return keyPress;
}
