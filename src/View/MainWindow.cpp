#include "MainWindow.hpp"






MainWindow::MainWindow(int width, int height, std::string title):
	RenderWindow(sf::VideoMode(width,height), title,sf::Style::Titlebar | sf::Style::Close),
	m_skin(),
	m_windowSize(),
	m_cellSize(105,105),
	m_gridSize(0,0),
	m_gridPosition(),
	m_spaceBetweenCell(15),
	m_font()
{
	//Set windows size
	m_windowSize=RenderWindow::getSize();
	
	m_gridPosition=sf::Vector2u(0,200);



	//Load font
	m_font.loadFromFile("./src/skin/original/Pragmatica-Medium.ttf");

	//Define original skin:
	m_skin.push_back(sf::Color(250,248,239));	//Background MainWindow
	m_skin.push_back(sf::Color(205,192,180));	//Background cells
	m_skin.push_back(sf::Color(187,173,160)); 	//Background grid color
	m_skin.push_back(sf::Color(119,110,101)); 	//2 and 4 font color
	m_skin.push_back(sf::Color(143,122,102)); 	//Button color
	m_skin.push_back(sf::Color(249,246,242)); 	//other number font Color
	m_skin.push_back(sf::Color(238,228,218,186)); 		//Game over color bg

	//Skin 2 et le 4
	m_skin.push_back(sf::Color(238,228,218)); //2
	m_skin.push_back(sf::Color(237,224,200)); //4

	//Skin 8 à 64
	m_skin.push_back(sf::Color(242,177,121)); 	//8
	m_skin.push_back(sf::Color(245,149,99));	//16
	m_skin.push_back(sf::Color(246,124,95)); 	//32
	m_skin.push_back(sf::Color(246,94,59)); 	//64

	//Skin 128 à 2048
	m_skin.push_back(sf::Color(237,207,114)); 	//128
	m_skin.push_back(sf::Color(237,204,97)); 	//256
	m_skin.push_back(sf::Color(237,200,80)); 	//512
	m_skin.push_back(sf::Color(237,197,63)); 	//1024
	m_skin.push_back(sf::Color(238,194,46)); 	//2048

	//Skin for other number
	m_skin.push_back(sf::Color(60,58,50)); 	//More than 2048

}


MainWindow::~MainWindow(){
}

void MainWindow::clearBG(){
	RenderWindow::clear(m_skin.at(0));
}


void MainWindow::drawGrid(std::vector<std::vector<int> > grid, bool gameIsOver){


	//First draw the grid
	int gridX=m_gridPosition.x;
	int gridY=m_gridPosition.y;

	m_gridSize.x=(grid.at(0).size()*m_cellSize.x)+(grid.at(0).size()*m_spaceBetweenCell)+m_spaceBetweenCell;
	m_gridSize.y=(grid.size()*m_cellSize.y)+(grid.size()*m_spaceBetweenCell)+m_spaceBetweenCell;

	//Center:
	m_gridPosition.x=m_windowSize.x/2-m_gridSize.x/2;
	m_gridPosition.y=220;


	//Draw the grid
	sf::RectangleShape gridShape(sf::Vector2f(m_gridSize.x,m_gridSize.y));
	gridShape.setFillColor(m_skin.at(2));
	gridShape.setPosition(gridX,gridY);
	RenderWindow::draw(gridShape);

	for(int i=0;i<grid.size();i++){

		for(int j=0;j<grid.at(0).size();j++){

			int cellX=(gridX+m_spaceBetweenCell)+j*(m_cellSize.x+m_spaceBetweenCell);
			int cellY=(gridY+m_spaceBetweenCell)+i*(m_cellSize.y+m_spaceBetweenCell);
			int value=grid.at(i).at(j);

			this->drawCell(cellX,cellY,value);

		}

	}

	if(gameIsOver)
		this->drawGameOver(gridX,gridY);
}


void MainWindow::drawCell(int x, int y, int value){

	//Init RectangleShape
	sf::RectangleShape cell(sf::Vector2f(m_cellSize.x, m_cellSize.y));

	//Define color, checking skin
	cell.setFillColor(this->getCellColor(value));

	//Set position
	cell.setPosition(x,y);

	//Draw the cell
	RenderWindow::draw(cell);


	std::stringstream valueStream;
	valueStream << value;

	std::string valueString(valueStream.str());

	int fontSize(m_cellSize.x/2);
	int fontSizeFact=15;
	if(value>=1024)
		fontSize-=fontSizeFact;
	int valueSize(valueString.size());


	int fontX=x+(m_cellSize.x/2)-((valueSize*(fontSize-20))/2);
	int fontY=y+(m_cellSize.y/2)-(fontSize/2)-9;


	sf::Text text;
	text.setFont(m_font);
	text.setStyle(sf::Text::Bold);
	text.setCharacterSize(fontSize);
	text.setString(valueString); 
	if(value==2 || value==4)
		text.setColor(m_skin.at(3));
	else
		text.setColor(m_skin.at(4));
	text.setPosition(fontX,fontY);

	if(value != 0)
		RenderWindow::draw(text);
}

sf::Color MainWindow::getCellColor(int value){

	//Id of the first cell color skin
	int idStart=7;

	if(value==0){
		return m_skin.at(1);
	}
	else if(value%2==0){
		int result(value);
		int id(idStart);
		while(result!=2){
			result/=2;
			id++;
		}
		return m_skin.at(id);
	}

	return m_skin.at(idStart+11);
}

void MainWindow::drawGameOver(int gridX, int gridY){
	
	sf::RectangleShape gridShape(sf::Vector2f(m_gridSize.x,m_gridSize.y));
	gridShape.setFillColor(m_skin.at(6));
	gridShape.setPosition(gridX,gridY);
	RenderWindow::draw(gridShape);
}


void MainWindow::drawATH(Stats stats){

	int titleX=m_gridPosition.x;
	int titleY=m_gridPosition.y-190;


	//==================== Draw title  ====================	
	sf::Text text;
	text.setFont(m_font);
	text.setStyle(sf::Text::Bold);
	text.setCharacterSize(80);
	text.setColor(m_skin.at(3));
	text.setPosition(titleX,titleY);
	text.setString("2048"); 

	RenderWindow::draw(text);


	//==================== Draw best score ====================

	int scoreAndBestScoreFontSize(20);

	int bestScoreSizeX=110;
	int bestScoreSizeY=60;
	int bestScoreX=m_gridPosition.x+m_gridSize.x-bestScoreSizeX;
	int bestScoreY=titleY+25;

	sf::RectangleShape bestScoreShape(sf::Vector2f(bestScoreSizeX,bestScoreSizeY));
	bestScoreShape.setFillColor(m_skin.at(2));
	bestScoreShape.setPosition(bestScoreX,bestScoreY);
	RenderWindow::draw(bestScoreShape);

	text.setString("BEST");
	text.setPosition(bestScoreX+bestScoreSizeY-scoreAndBestScoreFontSize-5,bestScoreY+12);
	text.setCharacterSize(scoreAndBestScoreFontSize-5);
	text.setColor(m_skin.at(7));

	RenderWindow::draw(text);

	//==================== Draw score  ====================
	
	int scoreX=bestScoreX-bestScoreSizeX-5;
	int scoreY=bestScoreY;
	int scoreSizeX=bestScoreSizeX;
	int scoreSizeY=bestScoreSizeY;
	int scoreLength=std::to_string(stats.getScore()).size();

	sf::RectangleShape scoreShape(sf::Vector2f(scoreSizeX,scoreSizeY));
	scoreShape.setFillColor(m_skin.at(2));
	scoreShape.setPosition(scoreX,scoreY);
	RenderWindow::draw(scoreShape);


	text.setString("SCORE");
	text.setPosition(scoreX+scoreSizeY-scoreAndBestScoreFontSize-10,scoreY+12);
	text.setCharacterSize(scoreAndBestScoreFontSize-5);
	text.setColor(m_skin.at(7));

	RenderWindow::draw(text);

	text.setString(std::to_string(stats.getScore()));
	text.setPosition(scoreX+scoreSizeY-((scoreLength+20)/2)-scoreAndBestScoreFontSize+10,scoreY+scoreSizeY - scoreAndBestScoreFontSize-10);
	text.setCharacterSize(scoreAndBestScoreFontSize);
	text.setColor(sf::Color::White);

	RenderWindow::draw(text);
}

void MainWindow::drawGame(std::vector<std::vector<int> > grid, bool gameIsOver, Stats stats){
	this->drawGrid(grid,gameIsOver);
	this->drawATH(stats);
}
