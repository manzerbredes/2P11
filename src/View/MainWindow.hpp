


#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include "../Model/Stats.hpp"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>


class MainWindow : public sf::RenderWindow{

	private:
		std::vector<sf::Color> m_skin;
		//int m_windowMargin;
		//int m_sizeCell;

		sf::Font m_font;
		//Coordonates
		sf::Vector2u m_windowSize;
		sf::Vector2u m_gridSize;
		sf::Vector2u m_cellSize;

		sf::Vector2u m_gridPosition;
		int m_spaceBetweenCell;

	public:
		MainWindow(int width, int height, std::string title);
		~MainWindow();

		void clearBG();
		void drawGrid(std::vector<std::vector<int> > grid, bool gameIsOver);
		void drawCell(int x, int y, int value);
		sf::Color getCellColor(int value);

		void drawGameOver(int gridX, int griY);

		void drawATH(Stats stats);

		void drawGame(std::vector<std::vector<int> > grid, bool gameIsOver, Stats stats);
};
