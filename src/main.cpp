//----- STD include -----
#include <iostream>
#include <string>
#include <time.h>
#include <tuple>
//----------------------

//----- Personnal include -----
#include "./Controllers/SFMLController/SFMLController.hpp"
//-----------------------------





//----- Start -----
int main()
{
    	//Init random
	srand(time(NULL));

	//Init controller
	SFMLController controller;

	//Run the game
	controller.run();

	//End the application
	return 0;
}
