#include "Grid.hpp"

//==================== Constructor and Destructor  ====================

//Constructor
Grid::Grid(): m_size(4), m_grid(4){

	//Init all cells
	for(int i=0;i<m_size;i++){
		for(int j=0;j<m_size;j++){
			m_grid.at(i).push_back(0);
		}
	}
	//m_grid.at(3).at(0)=2048;
}

//Destructor
Grid::~Grid(){
}


//==================== Merge and defragment methods  ====================

//Defragment the line to the right
std::vector<int> Grid::rightDefragment(std::vector<int> line){
	for(int j=0; j<m_size-1;j++){
		for(int i=0; i<m_size-1;i++){
			int val1=line.at(i);
			int val2=line.at(i+1);

			if(val1 != 0 && val2 == 0){
				line.at(i)=0;
				line.at(i+1)=val1;
			}
		}
	}
	return line;
}

//Defragment the line to the left using right defragmentation
std::vector<int> Grid::leftDefragment(std::vector<int> line){

	std::vector<int> reversedLine= this->reverseLine(line);

	return this->reverseLine(this->rightDefragment(reversedLine));
}

//Merge line to the right
std::vector<int> Grid::rightMerge(std::vector<int> line){
	for(int i=0; i< m_size-1;i++){
		int val1=line.at(i);
		int val2=line.at(i+1);

		if(val1==val2){
			line.at(i)=0;
			line.at(i+1)=val1*2;
			m_lastMoveScore+=val1*2;
			i++;
		}
	}
	return line;
}

//Merge line to the left
std::vector<int> Grid::leftMerge(std::vector<int> line){
	for(int i=m_size-1; i>0;i--){
		int val1=line.at(i);
		int val2=line.at(i-1);

		if(val1==val2){
			line.at(i)=0;
			line.at(i-1)=val1*2;
			m_lastMoveScore+=val1*2;
			i--;
		}
	}
	return line;
}


//==================== Swipe methods  ====================


//Swipe to the right
bool Grid::swipeRight(){

	m_lastMoveScore=0;
	bool moveDone=false;
	for(int i=0; i<m_size;i++){
		std::vector<int> swipedLine(this->rightDefragment(this->leftMerge(this->rightDefragment(m_grid.at(i)))));

		if(!this->compareLines(m_grid.at(i), swipedLine)){
			moveDone=true;
			m_grid.at(i)=swipedLine;
		}
	}
	return moveDone;
}

//Swipe to the left
bool Grid::swipeLeft(){
	m_lastMoveScore=0;
	bool moveDone=false;
	for(int i=0; i<m_size;i++){
		std::vector<int> swipedLine(this->leftDefragment(this->rightMerge(this->leftDefragment(m_grid.at(i)))));
		if(!this->compareLines(m_grid.at(i), swipedLine)){
			moveDone=true;
			m_grid.at(i)=swipedLine;
		}
	}
	return moveDone;
}


//Swipe to the top
bool Grid::swipeUp(){
	m_lastMoveScore=0;
	bool moveDone=false;
	for(int i=0; i<m_size;i++){
		std::vector<int> colVect=this->getCol(i);

		std::vector<int> swipedLine(this->leftDefragment(this->rightMerge(this->leftDefragment(colVect))));
		if(!this->compareLines(colVect, swipedLine)){
			moveDone=true;
			this->setCol(i,swipedLine);
		}
	}
	return moveDone;
}
//Swipe to the bottom
bool Grid::swipeDown(){
	m_lastMoveScore=0;
	bool moveDone=false;
	for(int i=0; i<m_size;i++){
		std::vector<int> colVect=this->getCol(i);

		std::vector<int> swipedLine(this->rightDefragment(this->leftMerge(this->rightDefragment(colVect))));

		if(!this->compareLines(colVect, swipedLine)){
			moveDone=true;
			this->setCol(i,swipedLine);
		}
	}
	return moveDone;
}

//==================== Helpers ====================

//Get the max len of a string of the numbers in the grid :
//	Exemple:
//		- 1 return 1
//		- 22 return 2
//		- 120 return 3
//		- 1000 return 4
//This method help to calculate the columns size
int Grid::maxStrLenInGrid(){
	int max=0;
	for(int i=0;i<m_size;i++){
		for(int j=0;j<m_size;j++){
			std::string number=std::to_string(m_grid.at(i).at(j));
			if(number.size() > max)
				max=number.size();
		}
	}
	return max;
}

//Test if the cell at (i,j) is empty
bool Grid::isEmpty(int i, int j){
	if(m_grid.at(i).at(j) == 0)
		return true;
	return false;
}

//Return a tuple that contain a random empty cell
std::tuple<int, int> Grid::getRandomEmptyCellCoord(){

	//Init list of candidate
	std::vector<std::tuple<int, int> > candidates;

	//Construct list of candidates
	for(int i=0;i<m_size;i++){
		for(int j=0;j<m_size;j++){
			if(this->isEmpty(i,j)){
				std::tuple<int, int> currentCandidate(i,j);
				candidates.push_back(currentCandidate);
			}
		}
	}

	//If no candidate available
	if(candidates.size() == 0)
		return std::tuple<int, int>(-1, -1);

	//Select the candidates
	int winnerIs(rand() % candidates.size());

	//Return the candidate
	return candidates.at(winnerIs);

}

//Reverse a line
std::vector<int> Grid::reverseLine(std::vector<int> line){
	std::vector<int> reversedLine;

	for(int j=m_size-1; j>=0;j--){
		reversedLine.push_back(line.at(j));
	}

	return reversedLine;
}

//Return true if the grid is full. False else.
bool Grid::isFull(){

	for(int i=0;i<m_size;i++){
		for(int j=0;j<m_size;j++){
			if(m_grid.at(i).at(j) == 0)
				return false;
		}
	}

	return true;
}

//Return true if the grid is full and no move can be performed. False else.
bool Grid::isOver(){

	if(!this->isFull())
		return false;

	for(int i=0;i<m_size;i++){
		for(int j=0;j<m_size-1;j++){
			if(m_grid.at(i).at(j) == m_grid.at(i).at(j+1))
				return false;
		}
	}

	for(int i=0;i<m_size;i++){
		std::vector<int> colVect(this->getCol(i));

		for(int j=0;j<m_size-1;j++){
			if(colVect.at(j) == colVect.at(j+1))
				return false;
		}
	}

	return true;


}

//Return true if line1 is equals to line2. False else.
bool Grid::compareLines(std::vector<int> line1, std::vector<int> line2){

	for(int i=0;i<m_size;i++){

		if(line1.at(i) != line2.at(i))
			return false;
	}
	return true;
}

//Return the description of the grid in a string.
//In other terms, the grid in ASCII
std::string Grid::description(){

	//Init stringstream description
	std::stringstream description;

	//Get max str len of the grid
	int maxStrLen=this->maxStrLenInGrid();

	//Start to write description
	std::stringstream gridBorder;
	for(int i=0;i<(maxStrLen+1)*4+1;i++){
		gridBorder<<"-";
	}
	description << std::endl << gridBorder.str() << std::endl;
	for(int i=0;i<m_size;i++){
		for(int j=0;j<m_size;j++){
			std::stringstream spaceCol;
			//Get number of space needed in cell
			int nbSpace=maxStrLen-std::to_string(m_grid.at(i).at(j)).size();

			for(int k=0;k<nbSpace;k++){
				spaceCol << " ";
			}
			if(m_grid.at(i).at(j) == 0)
				description <<  "|" << spaceCol.str() <<  " ";
			else
				description << "|"<< spaceCol.str()  << m_grid.at(i).at(j) ;
		}
		description << "|";
		description << std::endl;
	}
	description << gridBorder.str() << std::endl << std::endl;

	//Return description
	return description.str();
}

//==================== Getters and Setters  ====================

//Getter for m_lastMoveScore
int Grid::getLastMoveScore(){
	return m_lastMoveScore;
}


//Change value of cell with a tuple
bool Grid::setCell(std::tuple<int, int> coord, int value){
	int i=std::get<0>(coord);
	int j=std::get<1>(coord);

	if(i>=0 && i<m_size && j>=0 && j<m_size){
		m_grid.at(i).at(j)=value;
		return true;
	}

	return false;
}

//Change value of cell with int
bool Grid::setCell(int i, int j, int value){
	std::tuple<int, int> coord(i,j);
	return this->setCell(coord, value);
}

//Assign a vector to a column.
void Grid::setCol(int col, std::vector<int> colVect){
	for(int i=0;i<m_size;i++){
		m_grid.at(i).at(col)=colVect.at(i);
	}
}

//Retrieve a specific column.
std::vector<int> Grid::getCol(int col){

	std::vector<int> colVect;

	for(int i=0;i<m_size;i++){
		colVect.push_back(m_grid.at(i).at(col));
	}

	return colVect;
}


std::vector<std::vector<int> > Grid::getGrid(){
	return m_grid;
}
