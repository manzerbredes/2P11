#ifndef	DEF_GRID
#define	DEF_GRID

/* Grid.h
 * Defines the class Grid
 * A grid contains a table of cells the game will be set on
 * Creators : krilius, manzerbredes
 * Date : 29/04/2015 */

#include <iostream>
#include <sstream>
#include <vector>
#include <tuple>

class Grid
{
	private:
		//Members
	protected:
		std::vector<std::vector<int> > m_grid;
		int m_size;
		int m_lastMoveScore;

		//Private methods

	public:
		//Constructor and Destructor
		Grid();
		~Grid();

		//Defragment and merge methods
		std::vector<int> rightDefragment(std::vector<int> line);
		std::vector<int> leftDefragment(std::vector<int> line);
		std::vector<int> rightMerge(std::vector<int> line);
		std::vector<int> leftMerge(std::vector<int> line);

		int maxStrLenInGrid();
		//Swipe methods
		bool swipeRight();
		bool swipeLeft();
		bool swipeUp();
		bool swipeDown();

		//Helpers
		bool isFull();
		bool isOver();
		bool isEmpty(int i, int j);
		std::tuple<int, int> getRandomEmptyCellCoord();
		bool compareLines(std::vector<int> line1, std::vector<int> line2);
		std::vector<int> reverseLine(std::vector<int> line);
		std::string description();

		//Getters and Setters
		bool setCell(std::tuple<int, int> coord, int value);
		bool setCell(int i, int j, int value);
		std::vector<int> getCol(int col);
		void setCol(int col, std::vector<int> colVect);
		int getLastMoveScore();
		std::vector<std::vector<int> > getGrid();
};


#endif
