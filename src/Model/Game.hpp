#ifndef DEF_GAME
#define DEF_GAME

/* Game.h
 * Defines the class Game
 * A game allows a player to play. It contains a grid and pops numbers
 * Creators : krilius, manzerbredes
 * Date : 29/04/2015 */

#include <iostream>
#include <string>
#include "../Helpers/Keyboard.hpp"
#include "Grid.hpp"
#include "Stats.hpp"
#include <tuple>

class Game : public Grid
{
	private:
		//Members
		//Grid m_grid;
		Stats m_stats;
	public:
		//Constructor and Destructor
		Game();
		~Game();

		//Helpers
		bool swipe(kbdh::Direction direction);
		void coutGrid();
		void popRandomNumber();
		//bool isOver();

		//Getters and Setters
		Stats getStats();
		//int maxStrLenInGrid();
		//std::vector<std::vector<int> > getGrid();
};

#endif
