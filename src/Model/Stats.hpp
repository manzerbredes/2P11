#ifndef __STATS__
#define __STATS__

#include <iostream>





class Stats{

	private:
		int m_score;
		int m_nbMove;
	public:

		Stats();
		~Stats();
		void incScore(int value);
		void incnbMove();


		int getScore();
		int getNbMove();



};

#endif
