#include "Stats.hpp"


Stats::Stats() :
	m_score(0),
	m_nbMove(0)
{

}

Stats::~Stats(){
}



void Stats::incScore(int value){
	m_score+=value;
}
void Stats::incnbMove(){
	m_nbMove++;
}


int Stats::getScore(){
	return m_score;
}
int Stats::getNbMove(){
	return m_nbMove;
}
