#include "Game.hpp"


//==================== Constructor and Destructor  ====================

//Constructor
Game::Game() : Grid(), m_stats(){
}

//Destructor
Game::~Game(){
}

//==================== Helpers  ====================

//Swipe action
bool Game::swipe(kbdh::Direction direction){

	bool moveDone;

	switch(direction){

		case kbdh::Left:
			moveDone=swipeLeft();
			break;
		case kbdh::Right:
			moveDone=swipeRight();
			break;
		case kbdh::Up:
			moveDone=swipeUp();
			break;
		case kbdh::Down:
			moveDone=swipeDown();
			break;
	}

	if(moveDone){
		m_stats.incScore(m_lastMoveScore);
		m_stats.incnbMove();
		this->popRandomNumber();
	}

	return moveDone;
}


//Cout the grid
void Game::coutGrid(){
	std::cout << this->description();
}

//Return true if the game is lost. False else.
//bool Game::isOver(){
	//return m_grid.isOver();
//}

//Pop a random number on the grid
void Game::popRandomNumber(){
	std::tuple<int, int> coord(Grid::getRandomEmptyCellCoord());

	int percent=rand() % 100;

	int number;

	if(percent <= 10){
		number=4;
	}
	else{
		number=2;
	}


	Grid::setCell(coord, number);
}
//==================== Getters and Setter  ====================

//Retrieve the Score
Stats Game::getStats(){
	return m_stats;
}


//std::vector<std::vector<int> > Game::getGrid(){
	//return m_grid.getGrid();
//}


//int Game::maxStrLenInGrid(){
	//return m_grid.maxStrLenInGrid();
//}
