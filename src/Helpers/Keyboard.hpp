#ifndef DEF_MODELCONSTANTS
#define DEF_MODELCONSTANTS

/* ModelConstants.hpp
 * Constains constants and enums used in the whole model
 * Creators : krilius, manzerbredes
 * Date : 29/04/2015 */

namespace kbdh {

	//Key arrow
	enum Direction { Up, Down, Left, Right };
	//Key arrow typedef
	typedef enum Direction Direction;

}

#endif
