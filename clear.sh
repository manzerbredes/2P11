#!/bin/bash

path="$(dirname "$(readlink -f $0)")"

find "$path" -name "CMakeFiles" -exec rm -rf {} \;
find "$path" -name "CMakeCache.txt" -delete
find "$path" -name "cmake_install.cmake" -delete
find "$path" -name "Makefile" -delete

